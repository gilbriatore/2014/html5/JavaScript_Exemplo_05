function minhaFunc(){
    alert('Foi clicado!');
}    

function manipularElemento(){
    var e = document.getElementById("item2");   
    alert(e.innerHTML);
}

window.onload = function(){
    //manipularElemento();

    var btn = document.getElementById("btnPrincipal");
    btn.onclick = function(){
        manipularElemento();   
    }    
}

//$(document).ready(function(){
$(function(){ //forma abreviada
   manipularElemento();
);